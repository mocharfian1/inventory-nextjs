// pages/_app.tsx
import type { AppProps } from 'next/app';
import {NextUIProvider} from '@nextui-org/react';
import {useRouter} from 'next/router';
import { useLocation } from 'react-router-dom';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const location = useLocation();

  // console.log(location)

  return (
    <NextUIProvider navigate={router.push}>
      <Component {...pageProps} />
    </NextUIProvider>
  )
}

export default MyApp;