// app/providers.tsx
'use client'

import {NextUIProvider} from '@nextui-org/react'
import {useRouter, usePathname} from 'next/navigation'
import {Navbar, NavbarBrand, NavbarContent, NavbarItem, Link, Button} from "@nextui-org/react";


export function Providers({children}: { children: React.ReactNode }) {
    const router = useRouter();

    const pathName = usePathname()
    
    const menu = [
        {
            isActive: false,
            menuName: "Dashboard",
            pathName: "/dashboard"
        },
        {
            isActive: false,
            menuName: "Master",
            pathName: "/master"
        },
    ]

    return (
        <NextUIProvider navigate={router.push}>
            <Navbar>
                <NavbarBrand>
                    <p className="font-bold text-inherit">ARFIAN</p>
                </NavbarBrand>
                <NavbarContent className="hidden sm:flex gap-4" justify="center">
                    { menu.map(item =>{ return(
                        <NavbarItem isActive={pathName == item.pathName} key={item.pathName}>
                        <Link as={Link} color="foreground" href={ item.pathName }>
                            { item.menuName }
                        </Link>
                    </NavbarItem>
                    ) }) }
                </NavbarContent>
                <NavbarContent justify="end">
                    <NavbarItem className="hidden lg:flex">
                        <Link href="#">Login</Link>
                    </NavbarItem>
                    <NavbarItem>
                        <Button as={Link} color="primary" href="#" variant="flat">
                        Sign Up
                        </Button>
                    </NavbarItem>
                </NavbarContent>
            </Navbar>
            {children}
        </NextUIProvider>
    )
}